from django.shortcuts import render, redirect
from receipts.models import Receipt, ExpenseCategory, Account
from django.contrib.auth.decorators import login_required
from receipts.forms import ReceiptForm, ExpenseCategoryForm, AccountForm


@login_required
def receipt_list(request):
    receipt = Receipt.objects.filter(purchaser=request.user)
    context = {
        "receipt_list": receipt,
    }
    return render(request, "receipts/list.html", context)


@login_required
def create_receipt(request):
    if request.method == "POST":
        form = ReceiptForm(request.POST)
        if form.is_valid():
            receipt = form.save(False)
            receipt.purchaser = request.user
            receipt.save()
        return redirect("home")
    else:
        form = ReceiptForm()
        context = {
            "form": form,
        }
        return render(request, "receipts/create.html", context)


@login_required
def category_list(request):
    category_list = ExpenseCategory.objects.filter(owner=request.user)
    context = {
        "category_list": category_list
    }
    return render(request, "categories/category_lists.html", context)


@login_required
def account_list(request):
    account_list = Account.objects.filter(owner=request.user)
    context = {
        "account_list": account_list
    }
    return render(request, "accounts/account_lists.html", context)


@login_required
def create_category(request):
    form = ExpenseCategoryForm(request.POST)
    if form.is_valid():
        category = form.save(False)
        category.owner = request.user
        form.save()
        return redirect("category_list")
    else:
        form = ExpenseCategoryForm()
    context = {
        "form": form,
    }
    return render(request, "categories/create_category.html", context)


@login_required
def create_account(request):
    form = AccountForm(request.POST)
    if form.is_valid():
        category = form.save(False)
        category.owner = request.user
        form.save()
        return redirect("account_list")
    else:
        form = AccountForm()
    context = {
        "form": form,
    }
    return render(request, "accounts/create_account.html", context)
